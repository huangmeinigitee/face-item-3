package com.itheima.commen;

import lombok.Data;

/**
 * @author Gongjx
 * @date 2022/1/10
 */
@Data
public class LabelVlalue {
    private Integer value;
    private String label;
}
