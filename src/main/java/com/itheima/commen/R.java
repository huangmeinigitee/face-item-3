package com.itheima.commen;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class R<T> {

    private Integer code; //编码：0成功，其它数字为失败

    private Boolean flag;

    private String message; //错误信息

    private T data; //数据

    private Map map = new HashMap(); //动态数据

    public static <T> R<T> success(T object) {
        R<T> r = new R<T>();
        r.data = object;
        r.code = 0;
        r.flag = true;
        return r;
    }

    public static <T> R<T> error(String msg) {
        R r = new R();
        r.message = msg;
        r.code = 1;
        r.flag = false;
        return r;
    }

    public R<T> add(String key, Object value) {
        this.map.put(key, value);
        return this;
    }


}
