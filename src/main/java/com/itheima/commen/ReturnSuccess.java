package com.itheima.commen;

import lombok.Data;

@Data
public class ReturnSuccess<T> {
    private boolean success;

    public static <T> ReturnSuccess<T> success(){
        ReturnSuccess<T> returnSuccess=new ReturnSuccess<T>();
        returnSuccess.success=true;
        return returnSuccess;
    }

    public static <T> ReturnSuccess<T> error(){
        ReturnSuccess<T> returnSuccess=new ReturnSuccess<T>();
        returnSuccess.success=false;
        return returnSuccess;
    }
}
