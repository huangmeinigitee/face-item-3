package com.itheima.commen;

import lombok.Data;

import java.util.List;

@Data
public class PermissionsPages<T> {
    private Long counts;
    private Long pages;//总页数
    private List<T> list;
    private String page;//当前页码
    private String pagesize;//

    public PermissionsPages(Long counts, Long pages, List<T> list, String page, String pagesize) {
        this.counts = counts;
        this.pages = pages;
        this.list = list;
        this.page = page;
        this.pagesize = pagesize;
    }

    public PermissionsPages() {
    }
}
