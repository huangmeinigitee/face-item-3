package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.dto.PageComDto;
import com.itheima.pojo.Subjects;

/**
*
*/
public interface SubjectsService extends IService<Subjects> {

    PageComDto findPage(Integer page, Integer pagesize, String subjectName);
}
