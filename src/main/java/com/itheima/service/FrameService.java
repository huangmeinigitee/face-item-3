package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.commen.R;
import com.itheima.dto.UserDto;
import com.itheima.pojo.User;

/**
*
*/
public interface FrameService extends IService<User> {

    R login(String username, String password);

    void register(String username, String password);

    void password(String oldPassword, String newPassword);

    UserDto profile(Long userId);

}
