package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.dto.PageComDto;
import com.itheima.commen.ReturnSuccess;
import com.itheima.pojo.Companys;

/**
*
*/
public interface CompanysService extends IService<Companys> {

    ReturnSuccess updateState(Integer id, Integer state);

    PageComDto search(Integer page, Integer pageSize, String tags, String province, String city, String shortName, Integer state);
}
