package com.itheima.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.dto.PageComDto;
import com.itheima.pojo.Articles;

/**
*
*/
public interface ArticlesService extends IService<Articles> {

    PageComDto search(Integer page, Integer pagesize, String keyword, Integer state);
}
