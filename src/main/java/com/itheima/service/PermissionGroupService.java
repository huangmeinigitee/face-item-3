package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.pojo.PermissionGroup;

import java.util.List;

/**
*
*/
public interface PermissionGroupService extends IService<PermissionGroup> {


}
