package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.dto.PageComDto;
import com.itheima.pojo.SubjectsDirectorys;

/**
*
*/
public interface SubjectsDirectorysService extends IService<SubjectsDirectorys> {

    PageComDto findPage(Integer page, Integer pagesize, Integer subjectID, String directoryName, Integer state);
}
