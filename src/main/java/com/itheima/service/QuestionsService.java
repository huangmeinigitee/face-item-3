package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;

import com.itheima.dto.MenuDto;
import com.itheima.dto.QuestionsAuditOpinionsDto;
import com.itheima.dto.QuestionsChoice;
import com.itheima.dto.QuestionsDto;
import com.itheima.pojo.Questions;

/**
*
*/
public interface QuestionsService extends IService<Questions> {

    QuestionsAuditOpinionsDto auditOpinions(Integer id);
    QuestionsDto findPage(Integer page, Integer pagesize);

    String deleteById(Integer id);


    String addQuestionsAuditOpinions(QuestionsDto questionsAuditOpinionsDto);


    QuestionsDto findById(Integer id,Boolean isNext);

    String modiById(Integer id, QuestionsDto questionsDto);

//    QuestionsDto randoms(QuestionsChoice questionsChoice);

    String choiceStateById(Integer id, Boolean choiceState);

    String check(Integer id, Questions questions);
}
