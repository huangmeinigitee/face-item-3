package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.pojo.PermissionMenuExtend;

/**
*
*/
public interface PermissionMenuExtendService extends IService<PermissionMenuExtend> {

    String getCodeByPid(Long permissionMenuExtendId);
}
