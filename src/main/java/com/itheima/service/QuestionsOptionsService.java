package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.pojo.QuestionsOptions;

/**
*
*/
public interface QuestionsOptionsService extends IService<QuestionsOptions> {

}
