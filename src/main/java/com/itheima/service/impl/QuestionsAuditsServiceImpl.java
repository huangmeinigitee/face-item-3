package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.pojo.QuestionsAudits;
import com.itheima.service.QuestionsAuditsService;
import com.itheima.mapper.QuestionsAuditsMapper;
import org.springframework.stereotype.Service;

/**
*
*/
@Service
public class QuestionsAuditsServiceImpl extends ServiceImpl<QuestionsAuditsMapper, QuestionsAudits>
implements QuestionsAuditsService{

}
