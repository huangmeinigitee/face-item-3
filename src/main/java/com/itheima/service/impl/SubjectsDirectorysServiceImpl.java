package com.itheima.service.impl;

import com.aliyuncs.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.dto.PageComDto;
import com.itheima.mapper.SubjectsMapper;
import com.itheima.mapper.UserMapper;
import com.itheima.pojo.Subjects;
import com.itheima.pojo.SubjectsDirectorys;
import com.itheima.service.SubjectsDirectorysService;
import com.itheima.mapper.SubjectsDirectorysMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
*
*/
@Service
public class SubjectsDirectorysServiceImpl extends ServiceImpl<SubjectsDirectorysMapper, SubjectsDirectorys>
implements SubjectsDirectorysService{

    @Autowired
    private SubjectsDirectorysMapper subjectsDirectorysMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private SubjectsMapper subjectsMapper;

    @Override
    public PageComDto findPage(Integer page, Integer pagesize, Integer subjectID, String directoryName, Integer state) {

        LambdaQueryWrapper<SubjectsDirectorys> wrapper = new LambdaQueryWrapper<>();

        if(subjectID!=null){
            wrapper.eq(SubjectsDirectorys::getSubjectID ,subjectID);
        }
        if(directoryName!=null){
            wrapper.like(SubjectsDirectorys::getDirectoryName,directoryName);
        }
        if (state!=null){
            wrapper.eq(SubjectsDirectorys::getState,state);
        }

        IPage<SubjectsDirectorys> p = new Page<>(page,pagesize);
        IPage<SubjectsDirectorys> iPage = subjectsDirectorysMapper.selectPage(p, wrapper);
        List<SubjectsDirectorys> records = iPage.getRecords();
        for (SubjectsDirectorys record : records) {
            record.setUsername(userMapper.selectById(record.getCreatorID()).getUsername());
            record.setSubjectName(subjectsMapper.selectById(record.getSubjectID()).getSubjectName());

        }
        PageComDto<SubjectsDirectorys> pageSub = new PageComDto<>();
        pageSub.setCounts((int) iPage.getTotal());
        pageSub.setPages((int) iPage.getPages());
        pageSub.setPage((int) iPage.getCurrent());
        pageSub.setItems(records);
        pageSub.setPageSize((int) iPage.getSize());
        return pageSub;
    }
}
