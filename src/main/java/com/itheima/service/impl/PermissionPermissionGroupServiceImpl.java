package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.pojo.PermissionPermissionGroup;
import com.itheima.service.PermissionPermissionGroupService;
import com.itheima.mapper.PermissionPermissionGroupMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
*
*/
@Service
public class PermissionPermissionGroupServiceImpl extends ServiceImpl<PermissionPermissionGroupMapper, PermissionPermissionGroup>
implements PermissionPermissionGroupService{
    @Autowired
    private PermissionPermissionGroupMapper permissionPermissionGroupMapper;
    @Override
    public List<Long> getPids(Long id) {
        List<Long> pemissions=permissionPermissionGroupMapper.selectByPgid(id);
        return pemissions;
    }

    @Override
    public Boolean add(Long id, Long pid) {
        Boolean add=permissionPermissionGroupMapper.add(id,pid);
        return add;
    }

}
