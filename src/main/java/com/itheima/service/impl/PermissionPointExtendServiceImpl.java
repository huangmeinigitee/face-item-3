package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.pojo.PermissionPointExtend;
import com.itheima.service.PermissionPointExtendService;
import com.itheima.mapper.PermissionPointExtendMapper;
import org.springframework.stereotype.Service;

/**
*
*/
@Service
public class PermissionPointExtendServiceImpl extends ServiceImpl<PermissionPointExtendMapper, PermissionPointExtend>
implements PermissionPointExtendService{

    @Override
    public String getCodeByPid(Long permissionPointExtendId) {
        PermissionPointExtend byId = this.getById(permissionPointExtendId);
        return byId.getCode();
    }
}
