package com.itheima.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.itheima.dto.MenuDto;
import com.itheima.dto.QuestionsAuditOpinionsDto;
import com.itheima.dto.QuestionsDto;
import com.itheima.mapper.QuestionsOptionsMapper;
import com.itheima.pojo.Questions;
import com.itheima.pojo.QuestionsOptions;
import com.itheima.service.QuestionsService;
import com.itheima.mapper.QuestionsMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
*
*/
@Service
public class QuestionsServiceImpl extends ServiceImpl<QuestionsMapper, Questions>
implements QuestionsService{

    @Autowired
    private QuestionsMapper questionsMapper;

    @Autowired
    private HttpServletRequest request;

    @Override
    public QuestionsAuditOpinionsDto auditOpinions(Integer id) {
        /*//获取登录
        HttpSession session = request.getSession();
        String name = (String) session.getAttribute("/");
        Questions questions = questionsMapper.selectById(id);
        //封装数据
        MenuDto menuDto = new MenuDto();
        menuDto.setOpinion(questions.getChkremarks());
        menuDto.setChecker("超级管理员");
        menuDto.setChkTime(questions.getChkdate());*/
        //获取登录
        HttpSession session = request.getSession();
        String name = (String) session.getAttribute("/");
        Questions questions = questionsMapper.selectById(id);
        //封装数据
        QuestionsAuditOpinionsDto questionsDto = new QuestionsAuditOpinionsDto();
        questionsDto.setOpinion(questions.getChkremarks());
        questionsDto.setChecker("超级管理员");
        questionsDto.setChkTime(questions.getChkdate());
        return questionsDto;
    }
    @Override
    public QuestionsDto findPage(Integer page, Integer pagesize) {
        Page<Questions> p = new Page<>(page, pagesize);
        LambdaUpdateWrapper<Questions> wrapper = new LambdaUpdateWrapper<>();
        IPage<Questions> questionsIPage = questionsMapper.selectPage(p, wrapper);
        //封装数据
        QuestionsDto questionsDto = new QuestionsDto();
        questionsDto.setCounts((int) questionsIPage.getTotal());
        questionsDto.setPagesize((int) questionsIPage.getSize());
        questionsDto.setPages((int) questionsIPage.getPages());
        questionsDto.setPage((int) questionsIPage.getCurrent());
        questionsDto.setItems(questionsIPage.getRecords().toArray());
        return questionsDto;
    }


    @Override
    public String deleteById(Integer id) {

        LambdaQueryWrapper<QuestionsOptions> wrapepr = new LambdaQueryWrapper<>();
        wrapepr.eq(QuestionsOptions::getQuestionsId, id);
        questionsOptionsMapper.delete(wrapepr);

        questionsMapper.deleteById(id);
        return "删除成功";
    }

    @Autowired
    private QuestionsOptionsMapper questionsOptionsMapper;

    @Override
    public String addQuestionsAuditOpinions(QuestionsDto questionsAuditOpinionsDto) {
        //添加hm_questions表
        Questions questions = new Questions();
        BeanUtils.copyProperties(questionsAuditOpinionsDto, questions);
        int insert1 = questionsMapper.insert(questions);
        //添加hm_questions_options表
        int insert = 0;
        Object[] items = questionsAuditOpinionsDto.getItems();
        String s = JSON.toJSONString(items);
        List<QuestionsOptions> questionsOptions1 = JSON.parseArray(s, QuestionsOptions.class);

        for (QuestionsOptions questionsOptions : questionsOptions1) {
            //补全数据
            questionsOptions.setQuestionsId(questions.getId());
            insert = questionsOptionsMapper.insert(questionsOptions);
        }
        if (insert <= 0 || insert1 <= 0) {
            return "添加失败";
        }
        ;

        return "添加成功";
    }


    @Override
    public QuestionsDto findById(Integer id, Boolean isNext) {

        Questions questions = questionsMapper.selectById(id);
        QuestionsDto questionsDto = new QuestionsDto();
        BeanUtils.copyProperties(questions, questionsDto);
        //查询hm_questions_options表
        LambdaQueryWrapper<QuestionsOptions> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(QuestionsOptions::getQuestionsId, questions.getId());
        List<QuestionsOptions> questionsOptions = questionsOptionsMapper.selectList(wrapper);
        //封装数据
        questionsDto.setItems(questionsOptions.toArray());
        return questionsDto;
    }

    @Override
    public String modiById(Integer id, QuestionsDto questionsDto) {
        Questions questions = new Questions();
        BeanUtils.copyProperties(questionsDto, questions);
        //修改
        questionsMapper.updateById(questions);
        // todo 无需先删后增
        //删除hm_questions_options中Questionsid=id的数据
//        LambdaUpdateWrapper<QuestionsOptions> wrapper=new LambdaUpdateWrapper<>();
//        wrapper.eq(QuestionsOptions::getQuestionsid,questions.getId());
        // questionsOptionsMapper.delete(wrapper);
        //在hm_questions_options中新增questionsDto.getItems()
        //将Object[]类型的数值转为List<QuestionsOptions>(集合是长度可变的数值)
        String s = JSON.toJSONString(questionsDto.getItems());
        List<QuestionsOptions> questionsOptions = JSON.parseArray(s, QuestionsOptions.class);
        for (QuestionsOptions questionsOption : questionsOptions) {
            //补全数据
            questionsOption.setQuestionsId(questions.getId());
            questionsOptionsMapper.insert(questionsOption);
        }
        return "修改成功";
    }


 /*   @Override
    public QuestionsDto randoms(QuestionsChoice questionsChoice) {
        IPage<Questions> p = new Page<>(questionsChoice.getPage(), questionsChoice.getPagesize());
        LambdaQueryWrapper<Questions> wrapper = new LambdaQueryWrapper<>();
        IPage<Questions> questionsIPage = questionsMapper.selectPage(p, wrapper);
        //封装数据
        List<Questions> records = questionsIPage.getRecords();//本页数据
        long total = questionsIPage.getTotal();//总条数
        long current = questionsIPage.getCurrent();//当前页码
        long size = questionsIPage.getSize();//每页显示数
        long pages = questionsIPage.getPages();//总页数
        QuestionsDto questionsDto = new QuestionsDto();
        questionsDto.setCounts((int) total);
        questionsDto.setPages((int) pages);
        questionsDto.setPagesize((int) size);
        questionsDto.setPage((int) current);
        questionsDto.setItems(records.toArray());
        return questionsDto;
    }*/


    @Override
    public String choiceStateById(Integer id, Boolean choiceState) {
        Questions questions = questionsMapper.selectById(id);
        questions.setIschoice(true);
        questionsMapper.updateById(questions);
        return "操作成功";
    }


    @Override
    public String check(Integer id, Questions questions) {
        Questions questions1 = questionsMapper.selectById(id);
        questions1.setChkstate(questions.getChkstate());
        questions1.setChkremarks(questions.getChkremarks());
        questionsMapper.updateById(questions1);
        return "操作成功";
    }

}
