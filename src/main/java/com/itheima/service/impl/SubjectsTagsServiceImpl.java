package com.itheima.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.dto.PageComDto;
import com.itheima.mapper.SubjectsMapper;
import com.itheima.mapper.UserMapper;
import com.itheima.pojo.SubjectsTags;
import com.itheima.service.SubjectsTagsService;
import com.itheima.mapper.SubjectsTagsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
*
*/
@Service
public class SubjectsTagsServiceImpl extends ServiceImpl<SubjectsTagsMapper, SubjectsTags>
implements SubjectsTagsService{
    @Autowired
    private SubjectsTagsMapper subjectsTagsMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private SubjectsMapper subjectsMapper;
    @Override
    public PageComDto findPage(Integer page, Integer pagesize,String tagName,Integer state) {
        LambdaQueryWrapper<SubjectsTags> wrapper = new LambdaQueryWrapper<>();
        if(tagName!=null){
            wrapper.like(SubjectsTags::getTagName,tagName);
        }
        if (state!=null){
            wrapper.eq(SubjectsTags::getState,state);
        }
        IPage<SubjectsTags> p=new Page<>(page,pagesize);
        IPage<SubjectsTags> subjectsTagsIPage = subjectsTagsMapper.selectPage(p, wrapper);
        List<SubjectsTags> records = subjectsTagsIPage.getRecords();
        for (SubjectsTags record : records) {
            record.setUsername(userMapper.selectById(record.getCreatorId()).getUsername());
            record.setSubjectName(subjectsMapper.selectById(record.getSubjectID()).getSubjectName());
        }
        PageComDto<SubjectsTags> subjectsTagsPageComDto = new PageComDto<>();
        subjectsTagsPageComDto.setCounts((int) subjectsTagsIPage.getTotal());
        subjectsTagsPageComDto.setItems(subjectsTagsIPage.getRecords());
        subjectsTagsPageComDto.setPage((int) subjectsTagsIPage.getCurrent());
        subjectsTagsPageComDto.setPages((int) subjectsTagsIPage.getPages());
        subjectsTagsPageComDto.setPageSize((int) subjectsTagsIPage.getSize());
        return subjectsTagsPageComDto;
    }
}
