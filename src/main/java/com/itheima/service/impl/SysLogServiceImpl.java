package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.pojo.SysLog;
import com.itheima.service.SysLogService;
import com.itheima.mapper.SysLogMapper;
import org.springframework.stereotype.Service;

/**
*
*/
@Service
public class SysLogServiceImpl extends ServiceImpl<SysLogMapper, SysLog>
implements SysLogService{

}
