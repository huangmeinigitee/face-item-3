package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.dto.QuestionsRecordsDto;
import com.itheima.pojo.QuestionsRecords;
import com.itheima.service.QuestionsRecordsService;
import com.itheima.mapper.QuestionsRecordsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
*
*/
@Service
public class QuestionsRecordsServiceImpl extends ServiceImpl<QuestionsRecordsMapper, QuestionsRecords>
implements QuestionsRecordsService{
    @Autowired
    private QuestionsRecordsMapper questionsRecordsMapper;

    @Override
    public QuestionsRecordsDto setRecords(Integer id) {

        QuestionsRecords questionsRecords = questionsRecordsMapper.selectById(id);
        //封装
        QuestionsRecordsDto questionsRecordsDto = new QuestionsRecordsDto();
        questionsRecordsDto.setOperation(questionsRecords.getOperation());//操作
        questionsRecordsDto.setSetterID(questionsRecords.getSetterid());//出题人
        questionsRecordsDto.setSetTime(questionsRecords.getSettime());
        System.out.println(questionsRecordsDto);
        return questionsRecordsDto;
    }


    @Override
    public String publishState(Integer id, Integer publishState) {
        QuestionsRecords questionsRecords = new QuestionsRecords();
        questionsRecords.setSetterid(2);
        questionsRecords.setSettime(new Date());
        questionsRecords.setQuestionsid(id);
        if(publishState.equals("0")){
            questionsRecords.setOperation("下架");

        }else {
            questionsRecords.setOperation("上架");
        }
        questionsRecordsMapper.insert(questionsRecords);
        return "操作成功";
    }

}
