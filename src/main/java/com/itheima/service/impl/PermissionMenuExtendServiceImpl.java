package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.pojo.PermissionMenuExtend;
import com.itheima.service.PermissionMenuExtendService;
import com.itheima.mapper.PermissionMenuExtendMapper;
import org.springframework.stereotype.Service;

/**
*
*/
@Service
public class PermissionMenuExtendServiceImpl extends ServiceImpl<PermissionMenuExtendMapper, PermissionMenuExtend>
implements PermissionMenuExtendService{


    @Override
    public String getCodeByPid(Long permissionMenuExtendId) {
        PermissionMenuExtend byId = this.getById(permissionMenuExtendId);
        return byId.getCode();
    }
}
