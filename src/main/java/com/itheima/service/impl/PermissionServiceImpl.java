package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.pojo.Permission;
import com.itheima.service.PermissionService;
import com.itheima.mapper.PermissionMapper;
import org.springframework.stereotype.Service;

/**
*
*/
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission>
implements PermissionService{

}
