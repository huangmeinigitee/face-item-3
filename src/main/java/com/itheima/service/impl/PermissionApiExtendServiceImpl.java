package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.pojo.PermissionApiExtend;
import com.itheima.service.PermissionApiExtendService;
import com.itheima.mapper.PermissionApiExtendMapper;
import org.springframework.stereotype.Service;

/**
*
*/
@Service
public class PermissionApiExtendServiceImpl extends ServiceImpl<PermissionApiExtendMapper, PermissionApiExtend>
implements PermissionApiExtendService{

}
