package com.itheima.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.dto.UserPageResult;
import com.itheima.dto.UsersDto;
import com.itheima.mapper.PermissionGroupMapper;
import com.itheima.mapper.UserMapper;
import com.itheima.pojo.PermissionGroup;
import com.itheima.pojo.User;
import com.itheima.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.util.*;

@Service
@Transactional
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private PermissionGroupMapper permissionGroupMapper;


    //用户修改
    @Override
    public void modify(Long id, User user) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getId,id);
        user.setPermissionGroupId(user.getPermission_group_id());
        userMapper.update(user,wrapper);
    }

    //用户添加
    @Override
    public void add(User user) {
        /*LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getEmail,user.getEmail());
        User selectOne = userMapper.selectOne(wrapper);
        if (selectOne!=null){
            return R.error("用户email重复");
        }*/
        //设置默认密码123456
        user.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));
        //补全参数
        user.setCreateTime(new Date());
        user.setLastUpdateTime(new Date());
        user.setSex("男");
        user.setPermissionGroupId(user.getPermission_group_id());
        userMapper.insert(user);
        log.info("添加成功");
    }

    //用户列表
    @Override
    public UserPageResult listAll(Integer page, Integer pagesize, String username) {
        log.info("page==="+page);
        log.info("size==="+pagesize);
        log.info("username=="+username);
        IPage<User> p = new Page<>(page,pagesize);
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        //如果有分页查询条件
        if (username!= null&&username!=""){
            wrapper.like(User::getUsername,username);
        }
        wrapper.eq(User::getStatus,0);
        IPage<User> userIPage = userMapper.selectPage(p, wrapper);

        List<User> records = userIPage.getRecords();
        String jsonString = JSON.toJSONString(records);
        List<UsersDto> usersDtos = JSON.parseArray(jsonString, UsersDto.class);

        for (UsersDto usersDto : usersDtos) {
            PermissionGroup permissionGroup = permissionGroupMapper.selectById(usersDto.getPermissionGroupId());
            usersDto.setPermissionGroupName(permissionGroup.getTitle());
        }
        log.info("Dto=="+usersDtos.size());

        //封装返回对象
        UserPageResult userPageResult = new UserPageResult();
        userPageResult.setCounts(userIPage.getTotal());
        userPageResult.setPage(page);
        userPageResult.setPageSize(pagesize);
        userPageResult.setPages(userIPage.getPages());
        userPageResult.setList(usersDtos);
        return userPageResult;
    }

    //用户屏蔽、启用
    @Override
    public void modifyStatus(Long id, boolean disabled) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getId,id);
        User user = userMapper.selectOne(wrapper);
        //status true 屏蔽->1禁用  false 不屏蔽->0启用
        if (disabled){
            user.setStatus(1);
        }else {
            user.setStatus(0);
        }
        user.setLastUpdateTime(new Date());
        userMapper.updateById(user);
    }

    //用户简单列表
    @Override
    public List simpleList(String keyword, Integer disabled) {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(User::getUsername,keyword);
        if (disabled!=null){
            wrapper.eq(User::getStatus,disabled);
        }
        List<User> users = userMapper.selectList(wrapper);
        List items = new ArrayList();
        for (User user : users) {
            Map map =new HashMap();
            map.put("id",user.getId());
            map.put("fullName",user.getUsername());
            items.add(map);
        }
        return items;
    }

    //用户详情
    @Override
    public User userDetails(Long id) {
        User user = userMapper.selectById(id);
        user.setPermission_group_id(user.getPermissionGroupId());
        return user;
    }

    //用户重置密码
    @Override
    public void modifyPassword(Long id, String password) {
        User user = userMapper.selectById(id);
        String pwd = DigestUtils.md5DigestAsHex(password.getBytes());
        user.setPassword(pwd);
        user.setLastUpdateTime(new Date());
        userMapper.updateById(user);
    }
}
