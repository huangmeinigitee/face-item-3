package com.itheima.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.dto.PageComDto;
import com.itheima.commen.ReturnSuccess;
import com.itheima.mapper.CompanysMapper;
import com.itheima.pojo.Companys;
import com.itheima.service.CompanysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
*
*/
@Service
public class CompanysServiceImpl extends ServiceImpl<CompanysMapper, Companys>
implements CompanysService {
@Autowired
private CompanysMapper companysMapper;
    @Override
    public ReturnSuccess updateState(Integer id, Integer state) {
        Companys companys=new Companys();
        LambdaUpdateWrapper<Companys> wrapper = new LambdaUpdateWrapper<>();
        wrapper.eq(Companys::getId,id);
        wrapper.set(Companys::getState,state);
        int update = companysMapper.update(companys, wrapper);
        if(update>=0){
            return ReturnSuccess.success();
        }
        return ReturnSuccess.error();
    }

    @Override
    public PageComDto search(Integer page, Integer pageSize, String tags, String province, String city, String shortName, Integer state) {
        IPage<Companys> p=new Page<>(page,pageSize);
        LambdaQueryWrapper<Companys> wrapper=new LambdaQueryWrapper<>();
        if(tags!=null){
            wrapper.like(Companys::getTags,tags);
        }
        if(province!=null){
            wrapper.eq(Companys::getProvince,province);
        }
        if(city!=null){
            wrapper.eq(Companys::getCity,city);
        }
        if(shortName!=null){
            wrapper.like(Companys::getShortName,shortName);
        }
        if(state!=null){
            wrapper.eq(Companys::getState,state);
        }
        IPage<Companys> companysIPage = companysMapper.selectPage(p, wrapper);
        PageComDto pageComDto=new PageComDto();
        pageComDto.setCounts((int) companysIPage.getTotal());
        pageComDto.setItems(companysIPage.getRecords());
        pageComDto.setPage((int) companysIPage.getCurrent());
        pageComDto.setPages((int) companysIPage.getPages());
        pageComDto.setPageSize((int) companysIPage.getSize());
        return pageComDto;
    }
}
