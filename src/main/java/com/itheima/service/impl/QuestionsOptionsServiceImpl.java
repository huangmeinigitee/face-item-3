package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.pojo.QuestionsOptions;
import com.itheima.service.QuestionsOptionsService;
import com.itheima.mapper.QuestionsOptionsMapper;
import org.springframework.stereotype.Service;

/**
*
*/
@Service
public class QuestionsOptionsServiceImpl extends ServiceImpl<QuestionsOptionsMapper, QuestionsOptions>
implements QuestionsOptionsService{

}
