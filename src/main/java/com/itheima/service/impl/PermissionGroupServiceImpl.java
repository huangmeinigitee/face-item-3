package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.pojo.PermissionGroup;
import com.itheima.service.PermissionGroupService;
import com.itheima.mapper.PermissionGroupMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
*
*/
@Service
public class PermissionGroupServiceImpl extends ServiceImpl<PermissionGroupMapper, PermissionGroup>
implements PermissionGroupService{
    @Autowired
    private PermissionGroupMapper permissionGroupMapper;


}
