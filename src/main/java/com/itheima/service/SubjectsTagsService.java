package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.dto.PageComDto;
import com.itheima.pojo.SubjectsTags;

/**
*
*/
public interface SubjectsTagsService extends IService<SubjectsTags> {

    PageComDto findPage(Integer page, Integer pagesize, String tagName, Integer state);
}
