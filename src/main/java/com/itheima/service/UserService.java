package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.dto.UserPageResult;
import com.itheima.pojo.User;

import java.util.List;

public interface UserService extends IService<User> {
    void modify(Long id, User user);

    void add(User user);

    UserPageResult listAll(Integer page, Integer pagesize, String username);

    void modifyStatus(Long id, boolean disabled);

    List simpleList(String keyword, Integer disabled);

    User userDetails(Long id);

    void modifyPassword(Long id, String password);

}
