package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.pojo.PermissionPermissionGroup;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
*
*/
public interface PermissionPermissionGroupService extends IService<PermissionPermissionGroup> {

    List<Long> getPids(Long id);

    Boolean add(Long id, Long pid);
}
