package com.itheima.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.pojo.PermissionPointExtend;

/**
*
*/
public interface PermissionPointExtendService extends IService<PermissionPointExtend> {

    String getCodeByPid(Long permissionPointExtendId);
}
