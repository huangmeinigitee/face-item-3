package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.dto.QuestionsRecordsDto;
import com.itheima.pojo.QuestionsRecords;

/**
*
*/
public interface QuestionsRecordsService extends IService<QuestionsRecords> {

    QuestionsRecordsDto setRecords(Integer id);

    String publishState(Integer id, Integer publishState);
}
