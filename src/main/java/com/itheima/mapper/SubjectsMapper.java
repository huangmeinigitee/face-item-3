package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.Subjects;

/**
* @Entity com.itheima.pojo.Subjects
*/
public interface SubjectsMapper extends BaseMapper<Subjects> {


}
