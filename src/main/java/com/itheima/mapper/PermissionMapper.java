package com.itheima.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.Permission;
import org.apache.ibatis.annotations.Select;

/**
* @Entity com.itheima.pojo.Permission
*/
public interface PermissionMapper extends BaseMapper<Permission> {
    @Select("select name from pe_permission where type=1 and id=#{id}")
    String selectGetMenus(Long id);

    @Select("select name from pe_permission where type=2 and id=#{id}")
    String selectGetPoints(Long id);
}
