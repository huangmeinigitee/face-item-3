package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.SubjectsTags;

/**
* @Entity com.itheima.pojo.SubjectsTags
*/
public interface SubjectsTagsMapper extends BaseMapper<SubjectsTags> {


}
