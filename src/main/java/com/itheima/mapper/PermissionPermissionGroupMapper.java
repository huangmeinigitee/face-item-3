package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.PermissionPermissionGroup;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
* @Entity com.itheima.pojo.PermissionPermissionGroup
*/
public interface PermissionPermissionGroupMapper extends BaseMapper<PermissionPermissionGroup> {

    @Select("select pid from a_permission_permission_group where pgid=#{permissionGroupId}")
    List<Long> selectByPgid(Long permissionGroupId);

    @Insert("INSERT INTO a_permission_permission_group (`pgid`, `pid`) VALUES (#{id}, #{pid});")
    Boolean add(Long id, Long pid);
}
