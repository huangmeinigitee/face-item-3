package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.PermissionPointExtend;

/**
* @Entity com.itheima.pojo.PermissionPointExtend
*/
public interface PermissionPointExtendMapper extends BaseMapper<PermissionPointExtend> {


}
