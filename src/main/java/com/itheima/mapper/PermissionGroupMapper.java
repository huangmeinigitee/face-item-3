package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.PermissionGroup;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
* @Entity com.itheima.pojo.PermissionGroup
*/
public interface PermissionGroupMapper extends BaseMapper<PermissionGroup> {


}
