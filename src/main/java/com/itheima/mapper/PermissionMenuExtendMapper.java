package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.PermissionMenuExtend;

/**
* @Entity com.itheima.pojo.PermissionMenuExtend
*/
public interface PermissionMenuExtendMapper extends BaseMapper<PermissionMenuExtend> {


}
