package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.SysLog;

/**
* @Entity com.itheima.pojo.SysLog
*/
public interface SysLogMapper extends BaseMapper<SysLog> {


}
