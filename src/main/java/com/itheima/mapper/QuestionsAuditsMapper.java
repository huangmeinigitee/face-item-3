package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.QuestionsAudits;

/**
* @Entity com.itheima.pojo.QuestionsAudits
*/
public interface QuestionsAuditsMapper extends BaseMapper<QuestionsAudits> {


}
