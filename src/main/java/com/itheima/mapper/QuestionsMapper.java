package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.Questions;

/**
* @Entity com.itheima.pojo.Questions
*/
public interface QuestionsMapper extends BaseMapper<Questions> {


}
