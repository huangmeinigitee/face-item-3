package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.QuestionsRecords;

/**
* @Entity com.itheima.pojo.QuestionsRecords
*/
public interface QuestionsRecordsMapper extends BaseMapper<QuestionsRecords> {


}
