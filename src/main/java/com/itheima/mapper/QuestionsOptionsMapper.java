package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.QuestionsOptions;

/**
* @Entity com.itheima.pojo.QuestionsOptions
*/
public interface QuestionsOptionsMapper extends BaseMapper<QuestionsOptions> {


}
