package com.itheima.controller;

import com.itheima.dto.PageComDto;
import com.itheima.commen.ReturnSuccess;
import com.itheima.pojo.Companys;
import com.itheima.service.CompanysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestMapping("/companys")
public class CompanysController {
    @Autowired
    private CompanysService companysService;

    @GetMapping("/{id}")
    //id回显
    public Companys getCompanys(@PathVariable Integer id) {
        Companys byId = companysService.getById(id);
        return byId;

    }

    @PutMapping("/{id}")
    //修改
    public ReturnSuccess update(@RequestBody Companys companys) throws ParseException {
        boolean update = companysService.updateById(companys);
        if (update) {
            return ReturnSuccess.success();
        }
        return ReturnSuccess.error();
    }

    @PostMapping()
    //添加companys
    public ReturnSuccess add(@RequestBody Companys companys) {
        boolean save = companysService.save(companys);
        if (save) {
            return ReturnSuccess.success();
        }
        return ReturnSuccess.error();
    }

    @DeleteMapping("/{id}")
    //删除
    public ReturnSuccess delete(@PathVariable Integer id) {
        boolean remove = companysService.removeById(id);
        if (remove) {
            return ReturnSuccess.success();
        }
        return ReturnSuccess.error();
    }

    @PostMapping("/{id}/{state}")
    //状态修改
    public ReturnSuccess state(@PathVariable Integer id, @PathVariable Integer state) {
        ReturnSuccess returnSuccess = companysService.updateState(id, state);
        return returnSuccess;
    }

    @GetMapping()
    //查询
    public PageComDto search(Integer page, Integer pagesize,String tags,String province,String city,String shortName,Integer state) {
        PageComDto pageComDto=companysService.search(page,pagesize,tags,province,city,shortName,state);
        return pageComDto;
    }
}
