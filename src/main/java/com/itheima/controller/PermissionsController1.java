package com.itheima.controller;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.commen.PermissionsPages;
import com.itheima.commen.Result;
import com.itheima.dto.PermissionGroupDto;
import com.itheima.pojo.Permission;
import com.itheima.pojo.PermissionGroup;
import com.itheima.pojo.PermissionPermissionGroup;
import com.itheima.service.PermissionGroupService;
import com.itheima.service.PermissionPermissionGroupService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@Slf4j
public class PermissionsController1 {
    @Autowired
    private PermissionGroupService permissionGroupService;

    @Autowired
    private PermissionPermissionGroupService permissionPermissionGroupService;

    //id权限组详情
    @GetMapping("/permissions/{id}")
    public PermissionGroupDto get(@PathVariable Long id){
        //{返回值格式
        //    "id": 17,  --id
        //    "title": "12222",--name
        //    permissions":[37,41,42,52,59,60]
        //    "create_date": "2022-01-09T13:08:32.000Z"  createTime
        //}
        PermissionGroup permissionGroup = permissionGroupService.getById(id);
        log.info("找到PermissionGroup{}",permissionGroup);
        String s = JSON.toJSONString(permissionGroup);
        PermissionGroupDto permissionGroupDto = JSON.parseObject(s, PermissionGroupDto.class);
        permissionGroupDto.setCreate_date(permissionGroup.getCreateTime());
        permissionGroupDto.setUpdate_date(permissionGroup.getUpdateTime());
        List<Long> permissions =permissionPermissionGroupService.getPids(id);
        permissionGroupDto.setPermissions(permissions);
        return permissionGroupDto;


    }

    //修改权限组
    @PutMapping("/permissions/{id}")
    public Result update(@PathVariable Long id,@RequestBody PermissionGroupDto permissionGroupDto){
        // 3.修改权限组 1.删除原权限组集合 2.新增权限组集合
        //{
        //  "id": 17,
        //  "title": "12222",
        //  "permissions": []
        //}
        //3.找到 set 修改
        PermissionGroup permissionGroup = permissionGroupService.getById(id);
        permissionGroup.setUpdateTime(new Date());
        permissionGroup.setTitle(permissionGroupDto.getTitle());
        boolean b = permissionGroupService.updateById(permissionGroup);
        if (b) {
            log.info("修改权限组成功!");
        }else{
            log.info("修改权限组失败!");
        }

        //1.删除
        LambdaUpdateWrapper<PermissionPermissionGroup> luw=new LambdaUpdateWrapper<>();
        luw.eq(PermissionPermissionGroup::getPgid,id);
        boolean remove = permissionPermissionGroupService.remove(luw);
        if (remove) {
            log.info("删除原权限组成功!");
        }else{
            log.info("删除原权限组失败!");
        }
        //2.新增
        List<Long> pids = permissionGroupDto.getPermissions();
        if (pids != null) {
            pids.forEach(pid->{
                Boolean add=permissionPermissionGroupService.add(id,pid);
                if (add) {
                    log.info("新增权限组成功!");
                }
            });
        }
        return null;


    }

    //分页显示权限组
    @GetMapping("/permissions")
    public PermissionsPages<PermissionGroupDto> pages(Integer page,Integer pagesize,String title){
        /*Long counts;
        Integer pages;//总页数
        List<T> list;
        String page;//当前页码
        String pagesize;//*/
        IPage<PermissionGroup> pageInfo=new Page<>(page,pagesize);
        LambdaQueryWrapper<PermissionGroup> lqw=new LambdaQueryWrapper<>();
        lqw.like(title!=null,PermissionGroup::getTitle,title);
        IPage<PermissionGroup> page1 = permissionGroupService.page(pageInfo, lqw);
        PermissionsPages permissionsPages=new PermissionsPages(
                page1.getTotal(),
                page1.getSize(),
                page1.getRecords(),
                page+"",
                pagesize+""
        );
        return permissionsPages;

    }

    //新增权限组
    @PostMapping("/permissions")
    public Result add(@RequestBody Map map){
        log.info("拿到map!:{}",map);

        /*
        * {
    "title": "1111",
    "permissions": []
          }*/
        PermissionGroup permissionGroup=new PermissionGroup();
        permissionGroup.setTitle((String) map.get("title"));
        permissionGroup.setCreateTime(new Date());
        permissionGroup.setUpdateTime(new Date());
        log.info("新增permissionGroup:{}",permissionGroup);
        boolean save = permissionGroupService.save(permissionGroup);
        if (save) {
            log.info("新增权限组对象成功!");
        }else{
            log.info("新增权限组对象失败!");
        }
        List<Integer> permissions = (List) map.get("permissions");

        log.info("拿到pids!:{}",permissions);

        if (permissions != null) {
            permissions.forEach(pid->{
                Boolean add=permissionPermissionGroupService.add(permissionGroup.getId(),pid.longValue());
                if (add) {
                    log.info("新增权限组集合成功!");
                }
            });
        }
        return null;
    }

    //删除权限组
    @DeleteMapping("/permissions/{id}")
    public Result delete(@PathVariable Long id){
        //1.删除权限组集合 2.删除权限组
        LambdaUpdateWrapper<PermissionPermissionGroup> luw=new LambdaUpdateWrapper<>();
        luw.eq(PermissionPermissionGroup::getPgid,id);
        boolean remove = permissionPermissionGroupService.remove(luw);
        if (remove) {
            log.info("删除权限组集合成功!");
        }else{
            log.info("删除权限组集合失败!");
        }
        boolean b = permissionGroupService.removeById(id);
        if (b) {
            log.info("删除权限组对象成功!");
        }else{
            log.info("删除权限组对象失败!");
        }

        return null;
    }

    //权限组简单列表
    @GetMapping("/permissions/simple")
    public List simple(){
        //{返回值格式
        //    "id": 17,  --id
        //    "title": "12222",--name
        //    permissions":[37,41,42,52,59,60]
        //    "create_date": "2022-01-09T13:08:32.000Z"  createTime
        //}
        /*LambdaQueryWrapper<PermissionGroup> lqw=new LambdaQueryWrapper<>();
        lqw.eq(PermissionGroup::getTitle,keyword);
        PermissionGroup permissionGroup = permissionGroupService.getOne(lqw);
        log.info("找到PermissionGroup{}",permissionGroup);
        String s = JSON.toJSONString(permissionGroup);
        PermissionGroupDto permissionGroupDto = JSON.parseObject(s, PermissionGroupDto.class);
        permissionGroupDto.setCreate_date(permissionGroup.getCreateTime());
        permissionGroupDto.setUpdate_date(permissionGroup.getUpdateTime());
        List<Long> permissions =permissionPermissionGroupService.getPids(permissionGroup.getId());
        permissionGroupDto.setPermissions(permissions);*/
        List<PermissionGroup> list = permissionGroupService.list();
        return list;


    }

}
