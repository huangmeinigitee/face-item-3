package com.itheima.controller;

import com.itheima.dto.UserPageResult;
import com.itheima.pojo.User;
import com.itheima.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/users")
@Slf4j
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private HttpServletRequest request;

    //用户修改
    @PutMapping("/{id}")
    public void modify(@PathVariable Long id, @RequestBody User user) {
        log.info("id=="+id);
        log.info("user=="+user.getEmail());
        userService.modify(id,user);
    }

    //用户添加
    @PostMapping
    public void add(@RequestBody User user){
        log.info("user.getEmail--"+user.getEmail());
        userService.add(user);
    }

    //用户删除
    @DeleteMapping("/{id}")
    public void remove(@PathVariable Long id){
        boolean b = userService.removeById(id);
        /*if (b){
            return R.success("删除成功");
        }
        return R.error("删除失败");*/
    }

    //用户列表
    @GetMapping
    public UserPageResult listAll(Integer page,Integer pagesize,String username){
        UserPageResult userPageResult = userService.listAll(page,pagesize,username);
        return userPageResult;
    }

    //用户屏蔽、启用
    @PutMapping("/{id}/disabled")
    public void modifyStatus(@PathVariable Long id,boolean disabled){
        userService.modifyStatus(id,disabled);
    }

    //用户简单列表
    @GetMapping("/simple")
    public User simpleList(/*String keyword,Integer disabled*/){
        HttpSession session = request.getSession();
        Long userId = (Long) session.getAttribute("userId");
        User user = userService.getById(userId);
//        return userService.simpleList(keyword,disabled);
        return user;
    }

    //用户详情--修改回显
    @GetMapping("/{id}")
    public User userDetails(@PathVariable Long id){
        return userService.userDetails(id);
    }

    //用户重置密码
    @PutMapping("/{id}/password")
    public void password(@PathVariable Long id,String password){
        userService.modifyPassword(id,password);
    }
}
