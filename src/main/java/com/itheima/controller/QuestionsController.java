package com.itheima.controller;


import com.itheima.dto.MenuDto;
import com.itheima.dto.QuestionsAuditOpinionsDto;
import com.itheima.dto.QuestionsDto;
import com.itheima.dto.QuestionsRecordsDto;
import com.itheima.pojo.Questions;
import com.itheima.service.QuestionsRecordsService;
import com.itheima.service.QuestionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/questions")
public class QuestionsController {

    @Autowired
    private QuestionsService questionsService;
    //6.1审核意见
    @GetMapping("/{id}/auditOpinions")
    public QuestionsAuditOpinionsDto auditOpinions(@PathVariable Integer id){

      return questionsService.auditOpinions(id);
    }
    @Autowired
    private QuestionsRecordsService questionsRecordsService;


    //6.2出题记录
    @GetMapping("/{id}/setRecords")
    public QuestionsRecordsDto setRecords(@PathVariable Integer id) {

        return questionsRecordsService.setRecords(id);
    }

    //   6.3基础题库列表
    @GetMapping("")
    public QuestionsDto findPage(Integer page, Integer pagesize) {
        return questionsService.findPage(page, pagesize);
    }

    //6.4基础题库删除
    @DeleteMapping("/{id}")
    public String deleteById(@PathVariable Integer id) {

        return questionsService.deleteById(id);
    }

    //6.5基础题库添加
    @PostMapping
    public String addQuestionsAuditOpinions(@RequestBody QuestionsDto questionsAuditOpinionsDto) {
        return questionsService.addQuestionsAuditOpinions(questionsAuditOpinionsDto);
    }

    //6.5基础题库详情
    @GetMapping("/{id}")
    public QuestionsDto findById(@PathVariable Integer id, Boolean isNext) {

        return questionsService.findById(id, isNext);
    }

    //#6.6基础题库修改
    @PutMapping("/{id}")
    public String modiById(@PathVariable Integer id, @RequestBody QuestionsDto questionsDto) {
        return questionsService.modiById(id, questionsDto);
    }

    //#6.7批量导入题
    @PostMapping("/batch")
    public String batch(@RequestBody Questions questions) {
        return null;
    }



    //6.8精选题库上下架
    @PostMapping("/choice/{id}/{publishState}")
    public String publishState(@PathVariable Integer id, @PathVariable Integer publishState) {
        return questionsRecordsService.publishState(id,publishState);
    }

    //6.10组题列表
   /* @GetMapping("/randoms")
    public QuestionsDto randoms(@RequestBody QuestionsChoice questionsChoice){
        //找不到表
        return questionsService.randoms(questionsChoice);
    }*/

    //6.11组题列表删除
    @DeleteMapping("/randoms/{id}")
    public String  deleteByxx(@PathVariable Integer id){
        //找不到表
        return null;
    }

    //6.12加入或移出精选
    @PatchMapping("/choice/{id}/{choiceState}")
    public String choiceStateById(@PathVariable Integer id,Boolean choiceState){
        System.out.println(id+"id++++++++++++++++++++++");
        System.out.println(choiceState+"choiceState+++++++++++++++++");
        return questionsService.choiceStateById(id,choiceState);
    }


    //6.13试题审核
    @PostMapping("/check/{id}")
    public String check(@PathVariable Integer id,@RequestBody Questions questions){
        return questionsService.check(id,questions);
    }
}
