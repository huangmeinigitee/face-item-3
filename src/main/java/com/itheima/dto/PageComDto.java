package com.itheima.dto;

import lombok.Data;

import java.util.List;
@Data
public class PageComDto<T> {
    private Integer counts;
    private List<T> items;
    private Integer page;
    private Integer pages;
    private Integer pageSize;
}
