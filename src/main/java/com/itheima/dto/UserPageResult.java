package com.itheima.dto;

import com.itheima.pojo.User;
import lombok.Data;

import java.util.List;

@Data
public class UserPageResult {
    private Long counts;
    private Integer page;
    private Integer pageSize;
    private Long pages;
    private List<UsersDto> list;
}
