package com.itheima.dto;

import com.itheima.pojo.Articles;
import lombok.Data;

@Data
public class ArticlesDto extends Articles {
    private String username;
}
