package com.itheima.dto;

import com.itheima.pojo.User;
import lombok.Data;

@Data
public class UsersDto extends User {
    private String email;
    private String phone;
    private String username;
    private String permissionGroupName;
    private String role;
}
