package com.itheima.dto;

import com.itheima.pojo.Questions;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class QuestionsDto extends Questions {
      private  Integer counts;//总记录数,最大值: 5000 最小值: 100
      private Integer pagesize;//页大小最大值: 50 最小值: 5
      private Integer pages;//总页数 最大值: 100 最小值: 1
      private Integer page;//当前页码 最大值: 100 最小值: 1
      private Object[] items;//列表 最小数量: 10 元素是否都不同: true 最大数量: 20 item 类型: object



}
