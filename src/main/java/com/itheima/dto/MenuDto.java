package com.itheima.dto;

import com.itheima.pojo.Permission;
import lombok.Data;

import java.util.List;

@Data
public class MenuDto extends Permission {

    private Boolean is_point;
    private String  title;
    private String  code;
    private List<MenuDto> childs;
    private List<MenuDto> points;



}
