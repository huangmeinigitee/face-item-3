package com.itheima.dto;

import com.itheima.pojo.User;
import lombok.Data;

@Data
public class UserDto extends User {
    private String name; //用户名
    private String group; //部门
    private String signature; //签名
    private Roles roles;  //权限

    @Override
    public String toString() {
        return "UserDto{" +
                "name='" + name + '\'' +
                ", group='" + group + '\'' +
                ", signature='" + signature + '\'' +
                ", roles=" + roles +
                '}';
    }
}
