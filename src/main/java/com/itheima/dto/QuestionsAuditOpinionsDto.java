package com.itheima.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class QuestionsAuditOpinionsDto {
    private Object[] item;//item 类型: object
    private String opinion;//意见
    private LocalDateTime chkTime;//	时间
    private String checker;//审核人
}
