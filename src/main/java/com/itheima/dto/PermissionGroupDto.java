package com.itheima.dto;

import com.itheima.pojo.PermissionGroup;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class PermissionGroupDto<T> extends PermissionGroup {

    private Date create_date;
    private Date update_date;
    private List<T> permissions;

    public PermissionGroupDto(Date create_date, Date update_date, List<T> permissions) {
        this.create_date = create_date;
        this.update_date = update_date;
        this.permissions = permissions;
    }

    public PermissionGroupDto() {
    }
}
