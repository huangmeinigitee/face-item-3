package com.itheima.dto;


import com.itheima.pojo.Questions;
import lombok.Data;

@Data
public class QuestionsChoice extends Questions {
    private Integer page;//当前页数
    private Integer pagesize;//页尺寸
    private String  questionType;//试题类型
    private String keyword; //	关键字
    private String shortName;//企业简称
    private Integer creatorID;//录入人
    private Integer catalogID;//	目录
    private Integer chkState;//0 待审核 1 通过 2 拒绝

}
