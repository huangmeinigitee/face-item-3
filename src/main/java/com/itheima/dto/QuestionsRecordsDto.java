package com.itheima.dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

@Data
public class QuestionsRecordsDto {
    private Object[] item;//item 类型: object
    private String operation;//操作
    private Date setTime;//出题时间
    private Integer setterID;//出题人
}
