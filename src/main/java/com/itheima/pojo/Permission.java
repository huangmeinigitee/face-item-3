package com.itheima.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName pe_permission
 */
@TableName(value ="pe_permission")
@Data
public class Permission /*implements Serializable*/ {
    /**
     * 主键
     */
    @TableId
    private Long id;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 权限描述
     */
    private String description;

    /**
     * 权限名称
     */
    private String name;

    /**
     * 权限类型 1为菜单 2为功能 3为API
     */
    private Byte type;

    /**
     * 主键
     */
    private Long pid;

    /**
     * 主键ID
     */
    private Long permissionApiExtendId;

    /**
     * 主键ID
     */
    private Long permissionMenuExtendId;

    /**
     * 主键ID
     */
    private Long permissionPointExtendId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}