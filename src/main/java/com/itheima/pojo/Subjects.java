package com.itheima.pojo;


import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 学科
 * @TableName hm_subjects
 */
@TableName(value ="hm_subjects")
@Data
public class Subjects /*implements Serializable*/ {
    /**
     * 编号
     */
    @TableId
    private Integer id;

    /**
     * 学科
     */
    @TableField("subjectName")
    private String subjectName;

    /**
     * 创建者
     */
    @TableField("creatorID")
    private Integer creatorID;

    /**
     * 创建日期
     */
    @TableField("addDate")
    private Date addDate;

    /**
     * 前台是否显示
     */
    @TableField("isFrontDisplay")
    private Boolean isFrontDisplay;

    /**
     * 标签
     */
    private Integer tags;

    /**
     * 题目数量
     */
    private Integer totals;

    /**
     * 二级目录数
     */
    @TableField("twoLevelDirectory")
    private Integer twoLevelDirectory;

    @TableField(exist = false)
    private String username;
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}