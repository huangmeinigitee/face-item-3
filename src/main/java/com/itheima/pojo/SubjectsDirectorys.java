package com.itheima.pojo;


import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 学科-目录
 * @TableName hm_subjects_directorys
 */
@TableName(value ="hm_subjects_directorys")
@Data
public class SubjectsDirectorys /*implements Serializable*/ {
    /**
     * 编号
     */
    @TableId
    private Integer id;

    /**
     * 学科id
     */
    @TableField("subjectID")
    private Integer subjectID;

    /**
     * 目录名称
     */
    @TableField("directoryName")
    private String directoryName;

    /**
     * 创建者
     */
    @TableField("creatorID")
    private Integer creatorID;

    /**
     * 创建日期
     */
    @TableField("addDate")
    private Date addDate;

    /**
     * 面试题数量
     */
    private Integer totals;

    /**
     * 状态
     */
    private Integer state;

    @TableField(exist = false)
    private String username;

    @TableField(exist = false)
    private String subjectName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}