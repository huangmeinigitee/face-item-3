package com.itheima.pojo;


import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 企业管理
 * @TableName hm_companys
 */
@TableName(value ="hm_companys")
@Data
public class Companys /*implements Serializable*/ {
    /**
     * 编号
     */
    @TableId
    private Integer id;

    /**
     * 企业编号
     */
    private String number;

    /**
     * 是否名企
     */
    @TableField(value = "isFamous")
    private Boolean isFamous;

    /**
     * 企业简称
     */
    @TableField(value = "shortName")
    private String shortName;

    /**
     * 所属公司
     */
    private String company;

    /**
     * 省份
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 标签
     */
    private String tags;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 创建者
     */
    @TableField(value = "creatorID")
    private Integer creatorID;

    /**
     * 创建日期
     */
    @TableField(value = "addDate")
    private Date addDate;

    /**
     * 状态
     */
    private Integer state;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}