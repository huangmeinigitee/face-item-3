package com.itheima.pojo;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 
 * @TableName pe_permission_group
 */
@TableName(value ="pe_permission_group")
@Data
public class PermissionGroup /*implements Serializable*/ {
    /**
     * 主键ID
     */
    @TableId
    private Long id;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 权限组名称
     */
    @TableField("name")
    private String title;

    /**
     * 更新时间
     */
    private Date updateTime;



    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}