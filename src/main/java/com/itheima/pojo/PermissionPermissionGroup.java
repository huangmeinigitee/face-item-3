package com.itheima.pojo;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 
 * @TableName a_permission_permission_group
 */
@TableName(value ="a_permission_permission_group")
@Data
public class PermissionPermissionGroup /*implements Serializable*/ {
    /**
     * 权限组ID
     */

    private Long pgid;

    /**
     * 权限ID
     */
    private Long pid;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}