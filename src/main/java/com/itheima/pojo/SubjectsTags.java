package com.itheima.pojo;


import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 学科-标签
 * @TableName hm_subjects_tags
 */
@TableName(value ="hm_subjects_tags")
@Data
public class SubjectsTags /*implements Serializable*/ {
    /**
     * 编号
     */
    @TableId
    private Integer id;

    /**
     * 学科id
     */
    @TableField("subjectID")
    private Integer subjectID;

    /**
     * 标签名称
     */
    @TableField("tagName")
    private String tagName;

    /**
     * 创建者id
     */
    @TableField("creatorID")
    private Integer creatorId;

    /**
     * 创建日期
     */
    @TableField("addDate")
    private Date addDate;

    /**
     * 面试题数量
     */
    private Integer totals;

    /**
     * 状态
     */
    private Integer state;

    @TableField(exist = false)
    private String username;

    @TableField(exist = false)
    private String subjectName;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}