package com.itheima.pojo;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 
 * @TableName pe_permission_api_extend
 */
@TableName(value ="pe_permission_api_extend")
@Data
public class PermissionApiExtend /*implements Serializable*/ {
    /**
     * 主键ID
     */
    @TableId
    private Long id;

    /**
     * 权限等级，1为通用接口权限，2为需校验接口权限
     */
    private Integer apiLevel;

    /**
     * 请求类型
     */
    private String apiMethod;

    /**
     * 链接
     */
    private String apiUrl;

    /**
     * 主键
     */
    private Long permissionId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}