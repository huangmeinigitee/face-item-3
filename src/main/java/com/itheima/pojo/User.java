package com.itheima.pojo;


import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 
 * @TableName bs_user
 */
@TableName(value ="bs_user")
@Data
public class User /*implements Serializable*/ {
    /**
     * 主键
     */
    @TableId
    private Long id;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 介绍
     */
    private String introduction;

    /**
     * 最后修改时间
     */
    private Date lastUpdateTime;

    /**
     * 密码
     */
    private String password;

    /**
     * 手机号码
     */
    private String phone;

    /**
     * 账号状态 0为启用，1为禁用
     */
    private Integer status;

    /**
     * 用户名称
     */
    private String username;

    /**
     * 性别
     */
    private String sex;

    /**
     * 角色
     */
    private String role;

    /**
     * 权限组ID
     */
    private Long permissionGroupId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private Long permission_group_id;
}