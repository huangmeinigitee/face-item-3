package com.itheima.pojo;


import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 
 * @TableName pe_permission_point_extend
 */
@TableName(value ="pe_permission_point_extend")
@Data
public class PermissionPointExtend /*implements Serializable*/ {
    /**
     * 主键ID
     */
    @TableId
    private Long id;

    /**
     * 权限代码
     */
    private String code;

    /**
     * 主键
     */
    private Long permissionId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    public PermissionPointExtend(Long id, String code, Long permissionId) {
        this.id = id;
        this.code = code;
        this.permissionId = permissionId;
    }

    public PermissionPointExtend() {
    }
}