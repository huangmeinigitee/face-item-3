package com.itheima.pojo;


import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 试题出题记录
 * @TableName hm_questions_records
 */
@TableName(value ="hm_questions_records")
@Data
public class QuestionsRecords /*implements Serializable*/ {
    /**
     * 编号
     */
    @TableId
    private Integer id;

    /**
     * 问题id
     */
    private Integer questionsid;

    /**
     * 操作
     */
    private String operation;

    /**
     * 出题时间
     */
    private Date settime;

    /**
     * 出题人
     */
    private Integer setterid;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}