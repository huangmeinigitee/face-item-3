package com.itheima.pojo;


import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 
 * @TableName sys_log
 */
@TableName(value ="sys_log")
@Data
public class SysLog /*implements Serializable*/ {
    /**
     * 主键
     */
    @TableId
    private Long id;

    /**
     * method
     */
    private String method;

    /**
     * 操作时间
     */
    private Date operationDate;

    /**
     * 操作结果
     */
    private Boolean operationResult;

    /**
     * 参数内容
     */
    private String requestBody;

    /**
     * url
     */
    private String url;

    /**
     * 操作人ID
     */
    private Long userId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}