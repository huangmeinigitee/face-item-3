package com.itheima.pojo;


import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 基础题库-问题列表
 * @TableName hm_questions_options
 */
@TableName(value ="hm_questions_options")
@Data
public class QuestionsOptions /*implements Serializable*/ {
    /**
     * 编号
     */
    @TableId
    private Integer id;

    /**
     * 问题ID
     */
    @TableField("questionsID")
    private Integer questionsId;

    /**
     * 代码
     */
    private String code;

    /**
     * 标题
     */
    private String title;

    /**
     * 图片
     */
    private String img;

    /**
     * 是否正确答案
     */
    @TableField("isRight")
    private Boolean isRight;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}