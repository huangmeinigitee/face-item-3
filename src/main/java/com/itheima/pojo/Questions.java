package com.itheima.pojo;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 基础题库
 * @TableName hm_questions
 */
@TableName(value ="hm_questions")
@Data
public class Questions /*implements Serializable*/ {
    /**
     * 编号
     */
    @TableId
    private Integer id;

    /**
     * 试题编号
     */
    private String number;

    /**
     * 学科id
     */
    private Integer subjectid;

    /**
     * 目录id
     */
    private Integer catalogid;

    /**
     * 企业id
     */
    private Integer enterpriseid;

    /**
     * 省份
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 方向
     */
    private String direction;

    /**
     * 题型
     */
    private String questiontype;

    /**
     * 难度
     */
    private String difficulty;

    /**
     * 题干
     */
    private String question;

    /**
     * 解析视频
     */
    private String videourl;

    /**
     * 答案解析
     */
    private String answer;

    /**
     * 题目备注
     */
    private String remarks;

    /**
     * 试题标签
     */
    private String tags;

    /**
     * 精选题
     */
    private Boolean ischoice;

    /**
     * 发布状态
     */
    private Byte publishstate;

    /**
     * 发布时间
     */
    private Date publishdate;

    /**
     * 筛选状态
     */
    private Byte chkstate;

    /**
     * 审核人
     */
    private Integer chkuserid;

    /**
     * 审核意见
     */
    private String chkremarks;

    /**
     * 审核日期
     */
    private LocalDateTime chkdate;

    /**
     * 创建人id
     */
    private Integer creatorid;

    /**
     * 创建日期
     */
    private Date adddate;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}