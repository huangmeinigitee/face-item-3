package com.itheima.pojo;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;


@TableName(value ="hm_articles")
@Data
public class Articles /*implements Serializable*/ {
    /**
     * 编号
     */
    @TableId
    private Integer id;

    /**
     * 标题
     */
    private String title;

    /**
     * 文章正文
     */
    @TableField(value = "articleBody")
    private String articleBody;

    /**
     * 视频地址
     */
    @TableField(value = "videoURL")
    private String videoURL;

    /**
     * 阅读数
     */
    private Integer visits;

    /**
     * 状态
     */
    private Integer state;

    /**
     * 
     */
    @TableField(value = "createTime")
    private Date createTime;

    /**
     * 录入人
     */
    @TableField(value = "creatorID")
    private Integer creatorID;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}