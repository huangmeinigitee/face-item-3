package com.itheima.pojo;


import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 试题审核意见
 * @TableName hm_questions_audits
 */
@TableName(value ="hm_questions_audits")
@Data
public class QuestionsAudits /*implements Serializable*/ {
    /**
     * 编号
     */
    @TableId
    private Integer id;

    /**
     * 问题id
     */
    private Integer questionsid;

    /**
     * 意见
     */
    private String remarks;

    /**
     * 状态
     */
    private String operation;

    /**
     * 时间
     */
    private Date chktime;

    /**
     * 审核人
     */
    private Integer checkerid;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}